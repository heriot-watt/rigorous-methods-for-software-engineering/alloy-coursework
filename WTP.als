
let NB_SENSORS = 3

/*
//	--- Signatues ---
*/

// Subsystems

abstract sig WTP { states: set state }

one sig Console extends WTP { reset: Reset, count: alarm }
one sig Sensors extends WTP { sensors: set sensor }
one sig Alarm extends WTP { controlAlarm: alarm }
one sig Drain extends WTP { controlDrain: drain }

// Reset

one sig Reset { }

// States

abstract sig state { value: switch }
one sig alarm, drain extends state { }

abstract sig switch { }
one sig on, off extends switch { }

// Sensors

sig sensor { value: water_level }

abstract sig water_level { }
one sig low, normal , high extends water_level { }

/*
//	---  Predicates	---
*/

pred SensorsContain[s: Sensors, w: water_level] { w in s.sensors.value }

pred ControlAlarm [s: switch] { all a: Alarm | s in a.controlAlarm.value }
pred ControlDrain [s: switch] { all d: Drain | s in d.controlDrain.value }

/*
//	---  Facts	---
*/

fact {
	all s: state, w: WTP | s in w.states
	all s: Sensors | #s.sensors = NB_SENSORS
	all s: Sensors | some {high & s.sensors.value} =>  no {low & s.sensors.value}
}

fact {
	all s: Sensors | (SensorsContain[s, high] or SensorsContain[s, low]) => ControlAlarm[on] else ControlAlarm[off]
	all s: Sensors | SensorsContain[s, high] => ControlDrain[on] else ControlDrain[off]
}


run { }
